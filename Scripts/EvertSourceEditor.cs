﻿using UnityEngine;
using UnityEditor; // to extend Unity Editor
using System.Collections;

[CustomEditor(typeof(EvertSource))] 
public class EvertSourceEditor : Editor
{
	// There is a variable called 'target' that comes from the Editor, its the script we are extending but to
	// make it easy to use we will decalre a new variable called '_target' that will cast this 'target' to our script type
	// otherwise you will need to cast it everytime you use it like this: int i = (ourType)target;
	EvertSource _target;

	void OnEnable()
	{
		_target = (EvertSource)target;
	}

	public override void OnInspectorGUI()
	{
		GUILayout.BeginVertical();

		GUILayout.Label ("Evert audio source", EditorStyles.boldLabel);
		_target.playOnAwake =  EditorGUILayout.Toggle("Play On Awake", _target.playOnAwake); 
		_target.volume = EditorGUILayout.Slider("Volume", _target.volume, 0.0f, 10.0f); 
		_target.audioFileName = EditorGUILayout.TextField("Audio File Name", _target.audioFileName);

		// There is now ay to have a cake without chocolate
		if(_target.audioFileName == "")
		{    
			EditorGUILayout.HelpBox("Empty audio file name", MessageType.Warning);
		}    

		_target.directivity = (EvertSourceDirectivity)EditorGUILayout.EnumPopup("Directivity", _target.directivity);
		// _target.cakeT = (DoCake.cakeTypes)EditorGUILayout.EnumPopup("Cake type", _target.cakeT); // Enum Field - It needs proper casting
		//	_target.randomNumber = EditorGUILayout.IntField("Just a number", _target.randomNumber); // Common INT field
		//	_target.cakeColor = EditorGUILayout.ColorField("Color", _target.cakeColor); // Color Field

		//	if(GUILayout.Button("DO CAKE"))
		//	{
		//		_target.BakeTheCake();
		//	}

		GUILayout.EndVertical();

		//If we changed the GUI aply the new values to the script
		if(GUI.changed)
		{
			EditorUtility.SetDirty(_target);            
		}
	}
}