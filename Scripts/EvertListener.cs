﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvertListener : MonoBehaviour {

	private ThrottleUpdater throttleUpdater;

	void Awake() {
		throttleUpdater = new ThrottleUpdater (gameObject);
	}

	void Start () {
	}

	void Update () {
		// send position to evertims (throttle if not moved too much since last update)
		if (throttleUpdater.NeedsUpdate ()) {
			// Debug.Log ("update pos/rot " + EvertUtils.Vect3ToString(gameObject.transform.position));
		}
	}

}
