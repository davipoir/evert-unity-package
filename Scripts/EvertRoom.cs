﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; // StreamWriter (used for debug log)
using System; // for Split string

public class EvertRoom : MonoBehaviour {

	private MatFacesDescriptor matFacesDescr = new MatFacesDescriptor();

	void Start () {
		// loop over child objects
		if (transform.childCount > 0) {
			for (int iMesh = 0; iMesh < transform.childCount; iMesh++) {
				ProcessGameObject (transform.GetChild (iMesh).gameObject);
			}
		}
		// main object
		ProcessGameObject (gameObject);

		// write data to disk for check in Matlab
		matFacesDescr.ExportToDisk ();
	}

	void ProcessGameObject(GameObject gameObj){
		// sanity check: cant process static objects
		if (gameObj.isStatic) {
			Debug.LogError ("Evert cannot process room object: " + gameObj.name + " as it is set as 'static' (can't access triangle values of static object)");
			return;
		}
		// sanity check: if trying to process empty object
		if (gameObj.GetComponent ("MeshFilter") == null) {
			// Debug.LogWarning ("trying to access MeshFilter of " + gameObj.name + " while it has none");
			return;
		}

		// loop over go mesh sub-meshes (a sub-mesh per material)
		MeshFilter meshFilter = (MeshFilter)gameObj.GetComponent("MeshFilter");
		Mesh mesh = meshFilter.mesh;
		MeshRenderer meshRenderer = (MeshRenderer)gameObj.GetComponent("MeshRenderer");

		for (int iSubMesh = 0; iSubMesh < mesh.subMeshCount; iSubMesh++) {
			// Debug.Log ("material " + iSubMesh + ": " + meshRenderer.materials [iSubMesh].name);

			// get list of triangles IDs in sub-mesh
			int[] triangleIds = mesh.GetTriangles (iSubMesh);

			// loop over triangles IDs (groups of 3 ids) to get coordinates
			for (int iTriangle = 0; iTriangle < triangleIds.Length; iTriangle += 3) {
				Vector3 p1 = mesh.vertices [triangleIds [iTriangle + 0]];
				p1 = gameObj.transform.TransformPoint (p1);
				Vector3 p2 = mesh.vertices [triangleIds [iTriangle + 1]];
				p2 = gameObj.transform.TransformPoint (p2);
				Vector3 p3 = mesh.vertices [triangleIds [iTriangle + 2]];
				p3 = gameObj.transform.TransformPoint (p3);

				// get acoustic material name (assuming naming convention separating visual and acoustic material name with '__')
				string acousticMatName = meshRenderer.materials [iSubMesh].name.Split(new string[] { "__", " (Instance)" }, StringSplitOptions.None)[1];

				// register face
				matFacesDescr.AddFace (acousticMatName, iTriangle, new Vector3[]{ p1, p2, p3 });
			}
		}		
	}

	// Update is called once per frame
	void Update () {
		// draw ray 
		// Debug.DrawLine(listener.transform.position, source.transform.position, new Color(0.8F, 0.8F, 0.8F));
	}
}


[System.Serializable]
public class MatFacesDescriptor
{
	private Dictionary<string, List<Vector3[]>> matFaces = new Dictionary<string,  List<Vector3[]>>();

	public void AddFace(string matName, int faceId, Vector3[] triangles){
		if(!matFaces.ContainsKey(matName)){
			Debug.Log ("adding new material: " + matName);
			matFaces.Add (matName, new List<Vector3[]>());
		}
		matFaces [matName].Add (triangles);
		// todo: register faceId as well
	}

	public void ExportToDisk(){
		// init stream writer
		StreamWriter logWriter;
		string logPath = Path.Combine (Application.dataPath, "Matlab~");
		logWriter = new StreamWriter ( Path.Combine (logPath,"acousticMeshExport.txt") , false);

		// loop over mat dict
		foreach (KeyValuePair<string,List<Vector3[]>> matFace in matFaces) {
			foreach (Vector3[] p in matFace.Value) {
				string triStr = string.Format("{0:F2} {1:F2} {2:F2} {3:F2} {4:F2} {5:F2} {6:F2} {7:F2} {8:F2}", p[0].x, p[0].y, p[0].z, p[1].x, p[1].y, p[1].z, p[2].x, p[2].y, p[2].z);
				string line = matFace.Key + " " + triStr;
				logWriter.WriteLine(line);
			}
		}

		// close stream writer
		logWriter.Close ();
	}
		
}