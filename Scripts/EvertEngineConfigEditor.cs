﻿using UnityEngine;
using UnityEditor; // to extend Unity Editor
using System.Collections;

[CustomEditor(typeof(EvertEngineConfig))] 
public class EvertEngineConfigEditor : Editor
{
	EvertEngineConfig _target;

	void OnEnable()
	{
		_target = (EvertEngineConfig)target;
	}

	public override void OnInspectorGUI()
	{
		GUILayout.BeginVertical();

		GUILayout.Label ("Evert engine config", EditorStyles.boldLabel);

		_target.engineType = (EvertEngineType)EditorGUILayout.EnumPopup("Engine type", _target.engineType);

		if (_target.engineType == EvertEngineType.ISM) {
			_target.ismOrderMin = EditorGUILayout.IntSlider("ism order min", _target.ismOrderMin, _target.ISM_ORDER_MIN_VALUE, _target.ismOrderMax);
			_target.ismOrderMax = EditorGUILayout.IntSlider("ism order max", _target.ismOrderMax, _target.ismOrderMin, _target.ISM_ORDER_MAX_VALUE);
		}

		GUILayout.EndVertical();

		//If we changed the GUI aply the new values to the script
		if(GUI.changed)
		{
			EditorUtility.SetDirty(_target);            
		}
	}
}