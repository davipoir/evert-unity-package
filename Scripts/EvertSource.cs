﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EvertSourceDirectivity
{
	Omni,
	Cardioid
}

public class EvertSource : MonoBehaviour {

	public bool playOnAwake;
	public float volume;
	public string audioFileName;

	public EvertSourceDirectivity directivity;

	private ThrottleUpdater throttleUpdater;

	void Awake() {
		throttleUpdater = new ThrottleUpdater (gameObject);
	}

	void Start () {
		PrintAttr ();
		if (playOnAwake) { Play (); }
	}

	void Update () {
		// send position to evertims (throttle if not moved too much since last update)
		if (throttleUpdater.NeedsUpdate ()) {
			// Debug.Log ("update pos/rot " + EvertUtils.Vect3ToString(gameObject.transform.position));
		}
	}

	void Play(){
		// play audio source (send play msg through OSC)
		Debug.Log ("play audio source: " + audioFileName);
		OSC.Send ( new string[] {"source", gameObject.name, "start"}, 0);
	}

	void Stop(){
		// play audio source (send play msg through OSC)
		Debug.Log ("stop audio source: " + audioFileName);
	}

	void OnApplicationQuit(){
		Stop ();
	}

	void PrintAttr(){
		Debug.Log ("source name: " + gameObject.name);
		Debug.Log ("source volume: " + volume.ToString());
		Debug.Log ("source file: " + audioFileName);
		Debug.Log ("source directivity: " + EvertUtils.EvertDirectivityToString (directivity));
	}
}
