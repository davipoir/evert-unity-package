﻿using UnityEngine;
//using System; // to use Array
//using System.IO;
//using System.Linq; // to use array.ToList
//using System.Collections;
//using System.Collections.Generic; // to use Lists
//using UnityEngine.SceneManagement;

public static class EvertUtils {

	public static string Vect3ToString (Vector3 v, int roundFactor = 2){
		float g = 10.0F * (float)roundFactor;
		return (Mathf.Round (v.x * g) / g).ToString () + " " 
			+ (Mathf.Round (v.y * g) / g).ToString () + " " 
			+ (Mathf.Round (v.z * g) / g).ToString ();
	}

	public static string TransformToString (Transform t, int roundFactor = 2){
		return Vect3ToString (t.position) + " " + Vect3ToString (t.position);
	}

	public static float Round(float f, int roundFactor = 2){
		float g = Mathf.Pow(10.0F, (float)roundFactor);
		return Mathf.Round (f * g) / g;
	}

	public static string EvertDirectivityToString(EvertSourceDirectivity directivity){
		if (directivity == EvertSourceDirectivity.Omni) {
			return "Omni";
		}
		if (directivity == EvertSourceDirectivity.Cardioid) {
			return "Cardioid";
		}
		return "undefined";
	}

}

// Class used to limit updates on Evert object position / rotation
[System.Serializable]
public class ThrottleUpdater
{
	// local references
	private GameObject gameObject;

	// local attributes
	private Vector3 position;
	private Vector3 eulerAngles;
	private float threshPos; // in meters
	private float threshRot; // in degrees

	// constructor
	public ThrottleUpdater(GameObject _gameObject, float _threshPos = 0.1F, float _threshRot = 1.0F)
	{
		threshPos = _threshPos;
		threshRot = _threshRot;
		gameObject = _gameObject;

		position = Vector3.zero;
		eulerAngles = Vector3.zero;
	}

	// return true if gameObject moved / rotated "enought" since last call
	public bool NeedsUpdate(){
		bool needUpdateDist = Vector3.Distance (gameObject.transform.position, position) >= threshPos;
		bool needUpdateRot = Vector3.Distance (gameObject.transform.eulerAngles, eulerAngles) >= threshRot;
		// not sure the above is an angular distance per-se

		// Debug.Log (Vector3.Distance (newTransform.position, position));
		// Debug.Log( EvertUtils.Vect3ToString(newTransform.position) + "   " +  EvertUtils.Vect3ToString(position));

		// return true and update locals if update required
		if (needUpdateDist || needUpdateRot) {
			position = gameObject.transform.position;
			eulerAngles = gameObject.transform.eulerAngles;
			return true;
		} else {
			return false;
		}
	}

}
