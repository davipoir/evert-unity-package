﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EvertEngineType
{
	ISM,
	Raytracing
}

public class EvertEngineConfig : MonoBehaviour {

	public EvertEngineType engineType;
	public int ismOrderMin = 0;
	public int ismOrderMax = 3;

	public int ISM_ORDER_MAX_VALUE = 10;
	public int ISM_ORDER_MIN_VALUE = 0;

	void Start () {
		// send config to OSC
	}

	void OnApplicationQuit(){
		// stop OSC engine room acoustic simulation? 
	}

}
