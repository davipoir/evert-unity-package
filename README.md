# Unity package to setup an Evert scene
Unity scripts and prefabs to setup Evert auralization in a Unity scene.

## How to use
- Drag and drop folder into your project architecture
- Add ``EvertMain`` prefab to your scene
- Add ``EvertRoom``, ``EvertSource``, and ``EvertListener`` components to objects in your scene. You need at least one of each.