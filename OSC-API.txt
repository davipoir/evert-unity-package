-----------------------------------
EVERT OSC API
(prepend all with 'evert' to dissociate from other OSC msg?)
-----------------------------------

# allocate new room
/room/[roomID]/spawn

# flag room definition start (flush prev. definition)
/room/[roomID]/definestart

# allocate new face
/room/[roomID]/face/ [faceID]

# define face material
/room/[roomID]/face/[faceID]/material [matID]

# define face triangles (3 points xyz coordinates)
/room/[roomID]/face/[faceID]/triangles [p0_x p0_y p0_z p1_x p1_y p1_z p2_x p2_y p2_z]

# flag room definition over
/room/[roomID]/defineover

# de-allocate room
/room/[roomID]/destroy


# allocate new source
/source/[sourceID]/spawn

# define source transform matrix
/source/[sourceID]/transform [m00 m10 m20 m30 m01 m11 m21 m31 m02 m12 m22 m32 m03 m13 m23 m33]

# define source directivity
/source/[sourceID]/directivity [directivityType]

# destroy source
/source/[sourceID]/destroy


# allocate new listener
/listener/[listenerID]/spawn

# define listener transform matrix
/listener/[listenerID]/transform [m00 m10 m20 m30 m01 m11 m21 m31 m02 m12 m22 m32 m03 m13 m23 m33]

# destroy listener
/listener/[listenerID]/destroy


# define engine type
/engine/type [engineType]

# define ism max order
/engine/ism/orderMax [maxOrder]

# define ism min order
/engine/ism/orderMin [minOrder]


-----------------------------------
EVERT OSC API TYPES DEFINITION
-----------------------------------

[roomID]: string
[matID]: string
[faceID]: string
[sourceID]: string
[listenerID]: string
[directivityType]: string (predefined enum)
[p0_x p0_y p0_z p1_x p1_y p1_z p2_x p2_y p2_z]: floats
[m00 m10 m20 m30 m01 m11 m21 m31 m02 m12 m22 m32 m03 m13 m23 m33]: floats
[engineType]: string (predefined enum)
[maxOrder]: int
[minOrder]: int


-----------------------------------
AURALIZATION ENGINE OSC API
(not part of Evert framework)
(prepend all with 'auralization' to dissociate from other OSC msg?)
-----------------------------------

# define source audio input
/source/[sourceID]/input [sourceInputID]

# define source audio volume
/source/[sourceID]/volume [sourceVolume]

# start source playback (or enable adc input)
/source/[sourceID]/start [sourceStartTimeIn]

# stop source playback (or disable adc input)
/source/[sourceID]/stop

# loop source playback
/source/[sourceID]/loop [sourceIdLooping]


# define audio decoder (predefined in Max) 
global/decoder/id [decoderID]

# define active listenerID (redundant wih audio decoder?)
/listener/[listenerID]


# define global volume
global/volume [globalVolume]

# enable/disable dsp
global/dsp [dspStatus]


-----------------------------------
AURALIZATION ENGINE OSC API TYPES DEFINITION
-----------------------------------

[sourceInputID]: string (predefined)
[sourceVolume]: float
[sourceStartTimeIn]: float ('seek' like behavior)
[sourceIdLooping]: bool (0/1)
[decoderID]: int (predefined)
[globalVolume]: float
[dspStatus]: bool (0/1)


-----------------------------------
UNITY COORDINATE SYSTEM
-----------------------------------

from https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Matrix4x4.cs

        // memory layout:
        //
        //                row no (=vertical)
        //               |  0   1   2   3
        //            ---+----------------
        //            0  | m00 m10 m20 m30
        // column no  1  | m01 m11 m21 m31
        // (=horiz)   2  | m02 m12 m22 m32
        //            3  | m03 m13 m23 m33

where [m03 m13 m23] is [x y z]


from https://forum.unity.com/threads/how-to-assign-matrix4x4-to-transform.121966/

using UnityEngine;
 
public static class MatrixExtensions
{
    public static Quaternion ExtractRotation(this Matrix4x4 matrix)
    {
        Vector3 forward;
        forward.x = matrix.m02;
        forward.y = matrix.m12;
        forward.z = matrix.m22;
 
        Vector3 upwards;
        upwards.x = matrix.m01;
        upwards.y = matrix.m11;
        upwards.z = matrix.m21;
 
        return Quaternion.LookRotation(forward, upwards);
    }
 
    public static Vector3 ExtractPosition(this Matrix4x4 matrix)
    {
        Vector3 position;
        position.x = matrix.m03;
        position.y = matrix.m13;
        position.z = matrix.m23;
        return position;
    }
 
    public static Vector3 ExtractScale(this Matrix4x4 matrix)
    {
        Vector3 scale;
        scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
        scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
        scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
        return scale;
    }
}


-----------------------------------
OLD EVERTims API
-----------------------------------

/face id mat_id p0_x p0_y p0_z p1_x p1_y ... p3_y p3_z	update face <id> vertices (quad) coordinates and material

/facefinished	indicate room geometry update is complete (triggers new beam tree calculation)

/source id m00 m10 m20 m30 m01 ... m33	update source <id> position / rotation, with mij the coefficient of its 4x4 homogeneous matrix

/listener id m00 m10 m20 m30 m01 ... m33	update listener <id> position / rotation, with mij the coefficient of its 4x4 homogeneous matrix
